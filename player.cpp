#include "player.h"
#include "board.h"

/*
 ******************************************************************************
 ******************************************************************************
 *************************************     ************************************
 ************************************   *   ***********************************
 ************************************  ***  ***********************************
 ************************************       ***********************************
 ************************************  ***  ***********************************
 ************************************  ***  ***********************************
 ************************************  ***  ***********************************
 ******************************************************************************
 ******************************************************************************
 ***************    ***  **  ***     ***   ***  ***    ***      ***************
 **************  **  **  **  **   *   **    **  **  **  **  *******************
 **************  ******  **  **  ***  **  *  *  **  ******  *******************
 **************  ******      **       **  **    **  ******      ***************
 **************  ******  **  **  ***  **  ***   **  *    *  *******************
 **************  **  **  **  **  ***  **  ****  **  **   *  *******************
 ***************    ***  **  **  ***  **  ****  ***    * *      ***************
 ******************************************************************************
 ******************************************************************************
 */




/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    board = new Board();
    our_side = side;
	
	// Set the color of our opponents
	if (our_side == WHITE)
	{
		opp_side = BLACK;
	}
	else
	{
		opp_side = WHITE;
	}

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
// Take the opponent's move and update board
	board->doMove(opponentsMove, opp_side);
	
	if(opponentsMove)
	{	
		std::cerr << "Opponent moves (" << opponentsMove->getX() << ", " << opponentsMove->getY() << ")" << std::endl;
	}

// Decide what move to make
    Move * temp_move;
	int temp_score;
    Move * best_move = NULL;
	int best_score = -9999;
	Move * eventemper_move;
	if(board->hasMoves(our_side))
	{
	   for(int x = 0; x < 8; x++)
		{
			for(int y = 0; y < 8; y++)
			{
				temp_move = new Move(x, y);
				if(board->checkMove(temp_move, our_side))
				{
					temp_score = moveScore(board, our_side, temp_move);
					std::cerr << "Checking (" << x << ", " << y << "): " << temp_score << std::endl;
					if(temp_score > best_score)
					{
						std::cerr << "We have a new best move!" << std::endl;
						best_score = temp_score;
						eventemper_move = best_move;
						best_move = temp_move;
						temp_move = eventemper_move;
					}
				}
				if(temp_move != NULL)
				delete temp_move;
			}
		}
		board->doMove(best_move, our_side);
    	if (best_move != NULL)
		{
			std::cerr << "Our move (" << best_move->x << ", " << best_move->y << ")" << std::endl;
			return best_move;
		}
		std::cerr << "We're stuck here!" << std::endl;
		return NULL;
	}
    return NULL;
}

int Player::moveScore(Board *board, Side side, Move * move)
{
	int curr_score;
	// Copy board onto new temporary board
	Board * temp = board->copy();

	// Do move on new board
	temp->doMove(move, our_side);

	// Calculate new scores.
	curr_score = temp->count(our_side) - temp->count(opp_side);

	curr_score += Heuristic(move);

	delete temp;
	return curr_score;

// To add heuristic, we create a new function called Heuristic and it calculates how many points to add or subract from a move
}

int Player::Heuristic(Move *move)
{
	int counter = 0;
	int x1 = move->x;
	int y1 = move->y;
	if(x1 == 0 and y1 == 0)
	counter += 5;
	else if(x1 == 7 and y1 == 7)
	counter += 5;
	else if(x1 == 0 and y1 == 7)
	counter += 5;
	else if(x1 == 7 and y1 == 0)
	counter += 5;
	else if (x1 == 1 and y1 == 0)
	counter -= 1;
	else if (x1 == 1 and y1 == 1)
	counter -= 4;
	else if (x1 == 0 and y1 == 1)
	counter -= 1;
	else if (x1 == 6 and y1 == 7)
	counter -= 1;
	else if (x1 == 6 and y1 == 6)
	counter -= 4;
	else if (x1 == 7 and y1 == 6)
	counter -= 1;
	else if (x1 == 7 and y1 == 1)
	counter -= 1;
	else if (x1 == 6 and y1 == 1)
	counter -= 4;
	else if (x1 == 6 and y1 == 0)
	counter -= 1;
	else if (x1 == 0 and y1 == 6)
	counter -= 1;
	else if (x1 == 1 and y1 == 6)
	counter -= 4;
	else if (x1 == 1 and y1 == 7)
	counter -= 1;
	else if (x1 == 0 and y1 == 2)
	counter += 1;
	else if (x1 == 0 and y1 == 3)
	counter += 1;
	else if (x1 == 0 and y1 == 4)
	counter += 1;
	else if (x1 == 0 and y1 == 5)
	counter += 1;
	else if (x1 == 7 and y1 == 2)
	counter += 1;
	else if (x1 == 7 and y1 == 3)
	counter += 1;
	else if (x1 == 7 and y1 == 4)
	counter += 1;
	else if (x1 == 7 and y1 == 5)
	counter += 1;
	else if (y1 == 0 and x1 == 2)
	counter += 1;
	else if (y1 == 0 and x1 == 3)
	counter += 1;
	else if (y1 == 0 and x1 == 4)
	counter += 1;
	else if (y1 == 0 and x1 == 5)
	counter += 1;
	else if (y1 == 7 and x1 == 2)
	counter += 1;
	else if (y1 == 7 and x1 == 3)
	counter += 1;
	else if (y1 == 7 and x1 == 4)
	counter += 1;
	else if (y1 == 7 and x1 == 5)
	counter += 1;
	return counter;
}

