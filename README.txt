“Describe how and what each group member contributed for the past two weeks. If you are solo, these points will be part of the next section.”

There wasn’t much division of work between the two of us. The coding process
took place over 1 night, where both of us sat in Avery’s LFB lounge and coded the entire assignment together. We used Koding.com, which is a cloud
development environment, where we coded on a shared virtual environment.

The entire project was done collaboratively, where we would both discuss
the functions that we needed to implement until we were both on the same page.
Then, we’d typically code the section together on the Koding instance.

The only time I can recall where we really divided the work was when we were
implementing the heuristics. We had discussed how we wanted to score each tile,
and then created a skeleton. Jerry started from the bottom and John started from
the top and we just filled out the entire section that way.



“Document the improvements that your group made to your AI to make it tournament-worthy. Explain why you think your strategy(s) will work. Feel free to discuss any ideas were tried but didn't work out.”

I don’t really think that we made any improvements to the AI aside from the
very basic heuristics we created. We both decided that since we had enough
points to pass, we wanted to put as much focus in our finals as possible.